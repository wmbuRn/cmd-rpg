#ifndef MARKET_H_INCLUDED
#define MARKET_H_INCLUDED

#include <iostream>

class Market
{
    public:
        Market( int );
        ~Market();
       int returnMoney();

       void ClearCinBuffer();
       int DisplayMenuPage();
       void DisplayAlchemistScreen();
       void DisplayWeaponsmithScreen();
       void DisplayArmorerScreen();


    private:
        int numberOfSmallHpPotions;
        int numberOfMediumHpPotions;
        int priceOfSmallHpPotion;
        int priceOfMediumHpPotion;
        int HeroMoney;





};

#endif // MARKET_H_INCLUDED
