#ifndef HERO_H_INCLUDED
#define HERO_H_INCLUDED

#include <iostream>     /* cout, cin   */

#include "Market.h"
#include "Enemy.h"

class Hero
{
    private:
        int Strenght;
        int Agility;
        int Vitality;
        int Luck;
        int currentXP, maxXP;
        int minDMG, maxDMG;
        int Money;
        int Level;
        int currentHealth;

        int skillPoints;

        std::string Name;

    public:
        /*
            Constructors and d-cons
            We separated this from other functions in case we need copy and move operators
        */
        Hero();
        ~Hero();

        /*
            Getter function goes here, all data we want to get from private variables should go throught function
            I refuse to use int getVitality({return Vitality;})
            We will split everything to be visible and in their appropriate position.
        */
        int getVitality();
        int getStrenght();
        int getAgility();
        int GetLuck();
        int getMinimumDMG();
        int getMaximumDMG();
        int getLevel();
        int getHealth();
        int getCurrentXp();
        int getMaximumXp();
        int getMaximumHealth();
        int getCurrentHealth();
        int getMoney();
        int getMinimumCriticalDamage();
        int getMaximumCriticalDamage();
        int getBonusMoney();

        float getChanceToHIT();
        float getChanceForCritical();
        float getArmor();
        float getEvade();


        void CheckIfWeLeveled();
        void inputName();
        void LevelUp();
        void PrintOUT();
        void setMoney( int );
        void MainMenu();
        void Menu();
        void ClearCinBuffer();


};


#endif // HERO_H_INCLUDED
