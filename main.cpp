///////////////////////////////////////////////////////////////////////
//                                                                   //
//          Do with this code whatever you want.                     //
//    But first make shure that you are minimum LVL 70.              //
//                                                                   //
//                        CMD-RPG                                    //
//        The kind of a game we all played as kids                   //
//                                                                   //
//  Developed by: Stevan Kostic                                      //
//                                                                   //
//  Mail: Stevan.Kostic@live.com                                     //
//  Year: 2015                                                       //
//  Version: 0.2                                                     //
//                                                                   //
///////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Game is still in development, there is nothing past
// Character Screen.
// TO DO:
// 1. Main menu with Option to Start a New Game, Load Game - Base is implemented
//
// 2. Implement Fight system, random enemies, turn based combat
//
// 3. Implement Market where user can spend gold to buy potions
// and weapons                                             - Base is implemented
//
// 4. Implement Training menu where user can train his Character for Gold
//
// 5. Implement Bosses, Epic Fights, Epic Loot!
//
// 6. Implement Different Hero Classes like Warrior, Defender, Archer???
//
// 7. Implement Achievements!
//
// 8. Implement Save game



// Important
// DO IHERITANCE FROM BASE CLASS THAT WILL CALCULATE EVERYTHING AND
// MAKE 2 CLASSES HERO AND ENEMY INHERIT FROM IT




#include "Hero.h"

int main()
{
    Hero myHero;
    myHero.PrintOUT();

    return 0;

}
