#include "Market.h"

Market::Market(int Money)
{
    /*
        Money variable is coming from Hero class, we will latter return it when we finish
        with market Menu and feed the Hero::setGold( int );
    */
    HeroMoney = Money;

    numberOfSmallHpPotions = 4;
    priceOfSmallHpPotion = 150;

    DisplayMenuPage();

}

Market::~Market()
{
    // No need for d-con
}

void Market::ClearCinBuffer()
{
    std::cout << " " << std::endl;
    std::cout << "Please make a valid pick" << std::endl;

    /*
        In case the input is not a number we need to remove input from cin buffer
    */
    std::cin.clear();
    std::cin.ignore();
}

int Market::returnMoney()
{
    return HeroMoney;
}

int Market::DisplayMenuPage()
{
        std::cout << "1. Alchemist"                     << std::endl;
        std::cout << "2. Weaponsmith"                   << std::endl;
        std::cout << "3. Armorer"                       << std::endl;
        std::cout << " "                                << std::endl;
        std::cout << "0. Back To Character Screen"      << std::endl;
        int Input;
        std::cout << "I choose: ";
        std::cin >> Input;

        if ( Input == 0 )
        {
            // Lets return money to our character
            returnMoney();

            // In windows we he to return 0 and leave the scope of
            // function
            // Need prepocessor directive to include return 0 in windows builds
            return 0;

        }

        else if ( Input == 1 )
        {
            /*
                Display Alchemist menu
            */
            Market::DisplayAlchemistScreen();
        }

        else if ( Input == 2 )
        {
            /*
                Implement Weaponsmith menu
            */
        }

        else if ( Input == 3 )
        {
            /*
                Implement Armorer menu
            */
        }

        else
        {
            /*
                In case we didnt get input as number
            */
            Market::ClearCinBuffer();

        }

    /*
        If we reached here means that our DisplayMenuPage has failed
    */
    Market::DisplayMenuPage();
}

void Market::DisplayAlchemistScreen()
{
    int Input;
    std::cout << "Welcome Warrior"                                              << std::endl;
    std::cout << "You have: " << HeroMoney << "Gold"                            << std::endl;
    std::cout << "1. Buy Small Health Potion +50hp - 50 Gold"                   << std::endl;
    std::cout << "2. Buy Medium Health Potion + 100hp - 90 Gold"                << std::endl;
    std::cout << "3. Buy Fortitude Potion +10% Armor Bonus - 140 Gold"          << std::endl;
    std::cout << " "                                                            << std::endl;
    std::cout << "0. Return to Market Menu"                                     << std::endl;
    std::cout << "I choose: ";
    std::cin >> Input;

    if ( Input == 0 )
    {

        /*
            Let's return to previous menu screen
        */
        Market::DisplayMenuPage();
    }


    else if ( Input == 1 )
    {

        /*
            But first we need to check if we have enought money to buy this potion
        */
        if ( HeroMoney >= 50 )
        {
            /*
                We don't have inventory implemented so we will only take money from Hero like a good merchant.
            */
            HeroMoney -= 50;

        }

        else
        {
            /*
                Well we don't have enought money...
            */
            std::cout << "To bad you dont have enought money for that! Please come back when you have enought money" << std::endl;

        }
    }


    else if ( Input == 2 )
    {
        /*
            But first we need to check if we have enought money to buy this potion
        */
        if ( HeroMoney >= 90 )
        {
            /*
                We don't have inventory implemented so we will only take money from Hero like a good merchant.
            */
            HeroMoney -= 90;

        }

        else
        {
            /*
                Well we don't have enought money...
            */
            std::cout << "To bad you dont have enought money for that! Please come back when you have enought money" << std::endl;
        }
    }

    else if ( Input == 3 )
    {

        if ( HeroMoney >= 140 )
        {
            /*
                We don't have inventory implemented so we will only take money from Hero like a good merchant.
            */
            HeroMoney -= 140;

        }

        else
        {
            /*
                Well we don't have enought money...
            */
            std::cout << "To bad you don't have enought money for that! Please come back when you have enought money" << std::endl;

        }

    }


    else
    {
        /*
            In case the input is not a number we need to remove input from cin buffer
        */
        Market::ClearCinBuffer();

    }


    /*
        If we reached here means that our Market has failed
    */
    Market::DisplayAlchemistScreen();
}

void Market::DisplayWeaponsmithScreen()
{
    std::cout << "Welcome Warrior" << std::endl;
    std::cout << "You have: " << HeroMoney << " gold" << std::endl;
    /*
        We gonna need to generate weapons
    */

    std::cout << " 1. Buy Weapon";

}

