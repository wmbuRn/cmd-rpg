#include "Hero.h"

Hero::Hero()
{

    /*
        Basic stats for a hero, it would be changed if we implement hero classes
        and/or if we have luck attribute this will be random
        Update: Luck is here, need to change how other things are calculated.
    */
    Strenght                = 10;
    Agility                 = 10;
    Vitality                = 10;
    Luck                    = 5;

    /*
        We start from level 1, if we implement luck attribute starting level
        will be random
    */
    Level                   = 1;

    /*
        Same as level, if we have luck attribute staringXp will be random
    */
    currentXP               = 0;

    /*
        When we create character we want to have full health
    */
    currentHealth           = getMaximumHealth();

    /*
        We don't want our hero to be poor in the beginning?
        Also if we implement classes or Luck attribute money would be random
    */
    Money                   = 150;

    /*
        Starting skill points are 0, if we implement luck attribute and
        we leveled up on start this will be reimplemented
    */
    skillPoints             = 0;

    /*
        After we set out everything we want yer name!
        This will be disabled until mainMenu is done
    */
    // inputName();

    /*
        Display Main menu where user can pick to load a game or to start a new game
    */
    Hero::MainMenu();

    // testing how level up works, delete function call after debug
    // CheckIfWeLeveled();
}

void Hero::ClearCinBuffer()
{
    std::cout << "Please make a pick!" << std::endl;
    /*
        In case the input is not a number we need to remove input from cin buffer
    */
    std::cin.clear();
    std::cin.ignore();
}


void Hero::MainMenu()
{
    int Choice;
    std::cout << "1. Start New game!" << std::endl;
    std::cout << "2. Load Game!"      << std::endl;
    std::cout << "I choose: ";
    std::cin >> Choice;

    if ( Choice == 1 )
    {
        /*
            Start a New game menu by opening createNewCharacter menu??
        */
    }

    else if ( Choice == 2 )
    {
        /*
            Load DA game!
        */
    }

    else
    {
        /*
            In case input is not a number, we need to clear the cin buffer
        */
        Hero::ClearCinBuffer();

        /*
            Let's call mainMenu again with empty cin buffer
        */
        Hero::MainMenu();

    }

}

Hero::~Hero()
{
    /*
        No need for d-con
    */
}

int Hero::getStrenght()
{
    return Strenght;
}

int Hero::getVitality()
{
    return Vitality;
}

int Hero::getAgility()
{
    return Agility;
}

int Hero::getMinimumDMG()
{
    /*
        In case you swing like a wuss.
    */
    int DMG;
    DMG = (Strenght / 3) + (Agility / 5) + (Luck / 3);
    return DMG;
}

int Hero::getMaximumDMG()
{
    /*
        If you didn't do enought DMG swing harder!
    */
    int DMG;
    DMG = (Strenght / 2) + (Agility / 4) + (Luck / 3);
    return DMG;
}

void Hero::PrintOUT()
{
    /*
        Character Screen printOUT
        Think to add Termional colors!
        TO DO: CLEAN UP || Format text correctly!
        Reason: Battle Screen will only have some portion of hero stats
                so it would be better if all of this is in function and we only pick what we want to display
                hence implementing Market and to display Characters money would be much easier
    */
    std::cout << "Name: " << Name << std::endl;
    std::cout << "Level: " << Level << std::endl;
    std::cout << "Title: " << "Title will give additional bonuses - No implemented yet" << std::endl;
    std::cout << "Xp: " << getCurrentXp() << "/" << getMaximumXp() << std::endl;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "    Atributes   "  << std::endl;
    std::cout << "Strenght: " << getStrenght() << std::endl;
    std::cout << "Agility: " << getAgility() << std::endl;
    std::cout << "Vitality: " << getVitality() << std::endl;
    std::cout << "Luck: "     << GetLuck() << std::endl;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "    Damage Stats " << std::endl;
    std::cout << "Damage: " << getMinimumDMG() << "-" << getMaximumDMG() << std::endl;
    std::cout << "Chance to HIT: " << getChanceToHIT() << "%" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "    Critical Stats and Armor " << std::endl;
    std::cout << "Chance for Critical: " << getChanceForCritical() << "%" << std::endl;
    std::cout << "Critical Damage: " << getMinimumCriticalDamage() << "-" << getMaximumCriticalDamage() << std::endl;
    std::cout << "Armor: " << getArmor() << "% Damage reduction" << std::endl;
    std::cout << "Evade: " << getEvade() << "%" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "    Health Stats and Bonus " << std::endl;
    std::cout << "Health: " << currentHealth << "/" << getMaximumHealth() << std::endl;
    std::cout << "Bonus to Health: " << "-Not implemented yet!" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "    Money and Bounty " << std::endl;
    std::cout << "Money: " << getMoney() << std::endl;
    std::cout << "Bonus Money: " << getBonusMoney() << "%" << std::endl;
    std::cout << "Bounty On your Head: " << "- Not implemented Yet!" << std::endl;
    Hero::Menu();


}

void Hero::inputName()
{
    std::cout << "Hello young fellow, what your name would be?" << std::endl;
    std::cout << "My Name is: ";
    std::cin >> Name;
}

int Hero::getLevel()
{
    return Level;
}

int Hero::getCurrentXp()
{
    return currentXP;
}

int Hero::getMaximumXp()
{
    /*
        Lets start with basic calculation
        later we will improve this
    */
    maxXP = Level * 100;
    return maxXP;
}

int Hero::getMaximumHealth()
{
    return (Strenght / 2) + (Agility / 2) + (Vitality * 4);
}

int Hero::getCurrentHealth()
{
    return currentHealth;
}

int Hero::getMoney()
{
    return Money;
}

void Hero::CheckIfWeLeveled()
{
    if ( currentXP >= getMaximumXp() )
    {
        /*
            We are going to deduce maximumXp from currentXP and difference
            which would be our currentXP
            This way we can Level up more than once!
        */
        currentXP = currentXP - getMaximumXp();

        /*
            Each level gives up 3 skillpoints to spend
        */
        skillPoints += 3;

        /*
            We increase the level for 1
        */
        Level += 1;

        /*
            Recursive function call in case we leveled up more than 1 level
        */
        Hero::CheckIfWeLeveled();

        /*
            We will call LevelUp() atleast once when we level Up
        */
        Hero::LevelUp();
    }
}

void Hero::LevelUp()
{
    int pick;
    if ( skillPoints >= 1)
    {
        std::cout << "Badum-tssss, " << Name << " you have, " << skillPoints << " skill points so make your pick!" << std::endl;
        std::cout << "1. (Increase Strenght +1) - Affects Damage, Maximum Health and Armor"                      << std::endl;
        std::cout << "2. (Increase Agility +1) - Affects Armor, Damage and Maximum Health"                       << std::endl;
        std::cout << "3. (Increase Vitality +1) - Affects Maximum Health, and Armor"                             << std::endl;
        std::cout << "I choose: ";
        std::cin >> pick;
            if ( pick == 1 )
            {
                Strenght += 1;
                skillPoints -= 1;
                Hero::LevelUp();
            }
            else if ( pick == 2 )
            {
                Agility += 1;
                skillPoints -= 1;
                Hero::LevelUp();
            }
            else if ( pick == 3 )
            {
                Vitality += 1;
                skillPoints -= 1;
                Hero::LevelUp();
            }

            else
            {
                /*
                    In case the input is not a number we need to remove input from cin buffer
                */
                Hero::ClearCinBuffer();

                /*
                    Call LevelUp() again with cleared cin
                */
                Hero::LevelUp();
            }
    }

}

void Hero::setMoney(int newMoney )
{
    Money = newMoney;
}

void Hero::Menu()
{
    int select;
    std::cout << " " << std::endl;
    std::cout << "----------------------------------------------------------------------------" << std::endl;
    std::cout << "1. Fight"                 << std::endl;
    std::cout << "2. Market"                << std::endl;
    std::cout << "3. Training"              << std::endl;
    std::cout << " "                        << std::endl;
    std::cout << "0. Save&Quit"             << std::endl;
    std::cout << "I choose: ";
    std::cin >> select;

    if ( select == 0 )
    {
        /*
            Implement saving
        */
    }

    if ( select == 1 )
    {
        /*
            Open fight menu
            Think: To be part of Hero Class or New Class?
        */
    }

    else if ( select == 2 )
    {
        /*
            Market is different class, we need to feed it with value of variables we have in our Hero classs
        */
        Market shop(Hero::getMoney());

        /*
            When shop is done it returned all the money we have left after buying/selling on market
        */
        Hero::setMoney(shop.returnMoney());

        /*
            We are back to character screen, lets print everything and allow the user to travel across menus
        */
        Hero::PrintOUT();
    }

    else if( select == 3 )
    {
        /*
            Open training menu
            Think: To be part of Hero Class or New Class?
        */
    }

    else
    {

        /*
            In case the input is not a number we need to remove input from cin buffer
        */
        Hero::ClearCinBuffer();
    }

    /*
        If we reached here means that our Hero menu has failed
    */
    Hero::Menu();
}

float Hero::getArmor()
{
    /*
        We are going to calculate how much armor will reduce receiving damage
    */
    return (Agility / 3) + (Strenght / 4);
}

float Hero::getChanceToHIT()
{
    float chanceForHIT;
    /*
        Let's calculate what is our chance at hitting same level opponent
        TO DO: Implement different % to HIT when attacking different type on enemies
        and levels
    */
    chanceForHIT = (Luck * 4) + (Agility * 3) + (Strenght * 2) - (Level * 7);

    /*
        We need to set maximum chance to hit enemy
    */
    if ( chanceForHIT > 95)
    {
        return 95;
    }

    /*
        We need to set minimum chance to hit enemy, and
        let's sat it would be 35% of the time
    */
    else if ( chanceForHIT < 35)
    {
        return 35;
    }

    /*
        There is no need for else becouse if all IF's fails
        there is no other way then to return value that is between
        95 and 35
    */
    return chanceForHIT;
}

float Hero::getChanceForCritical()
{
    float chanceForCrit;
    /*
        chanceForCritical HIT will not change when attacking enemies that have different level than us, thus making Dexterity Hero build a good option
    */
    chanceForCrit = (Luck * 3) + Agility - (Level * 8);

    /*
        We need to CAP what is maximum chance for critical
    */
    if ( chanceForCrit > 55)
    {
        return 55;
    }

    /*
        We need to define minimum chance for critical
    */
    else if ( chanceForCrit < 10)
    {
        return 10;
    }

     /*
        There is no need for else becouse  if all IF's fails
        there is no other way then to return value that is between
        95 and 10
    */
    return chanceForCrit;
}

int Hero::getMinimumCriticalDamage()
{
    /*
        Minimum critical DMG our hero will do
        TO DO: Implement bonus per Level, Bonus when attacking wounded enemies,
        Luck modifier, Perk skill
    */
    return ( getMinimumDMG() * 1.5 ) + ( Strenght / 2 );
}

int Hero::getMaximumCriticalDamage()
{
     /*
        Maximum critical DMG our hero will do
        TO DO: Implement bonus per Level, Bonus when attacking wounded enemies,
        Luck modifier, Perk skill
    */
    return ( getMaximumDMG() * 2 ) + Strenght + ( Agility / 2 );
}

float Hero::getEvade()
{
    /*
        Evade skill define a % of which attacks we will skip and receive 0 DMG
        TO DO: Implement Counter-Attack Initiative, PERK skill
    */
    float Evade;
    Evade = (( Luck * 3 ) +  Agility ) - (Level * 10);

    /*
        Need to set maximum evade so that we don't evade all the time
    */
    if ( Evade > 95 )
    {
        return 95;
    }

    /*
        Minimum evade that we can't go under
    */
    else if ( Evade < 15 )
    {
        return 15;
    }

    /*
        There is no need for Else, becouse return argument breaks out of function call
        if all IF's fails
        there is no other way then to return value that is between
        95 and 15
    */
    return Evade;
}

int Hero::GetLuck()
{
    return Luck;
}

int Hero::getBonusMoney()
{
    int bonusMoney;
    bonusMoney = ( Luck * 5 ) - ( Level * 4 );

    /*
        Lets set the maximum bonus we can have on earned money
    */
    if ( bonusMoney > 200 )
    {
        return 200;
    }

    /*
        Let's set how much is minimum we will get from money we earn in battles
    */
    else if ( bonusMoney < 15 )
    {
        return 15;
    }

    /*
        if all IF's fails there is no other way then to return value that is between
        200 and 15
    */
    return bonusMoney;
}
