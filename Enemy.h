#ifndef ENEMY_H_INCLUDED
#define ENEMY_H_INCLUDED

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time        */

class Enemy
{
    public:
        Enemy( int, int, int, int, int, int );
        ~Enemy();
        int CalculateMinimumDMG();
        int CalculateMaximumDMG();
        int generateRandomNumbers( int );
        int getMaximumHealth();
        int getArmor();
        int getMinimumDMG();
        int getMaximumDMG();
        int getChanceToHIT();
        int getCurrentHealth();





    private:
        int Strenght;
        int Level;
        int Gold;
        int currentHealth;
        int Dexterity;
        int Vitality;
        int Luck;
        int minDMG, maxDMG;

        /*
            We will get this stats from Hero class
        */
        int HeroLevel;
        int HeroStrenght;
        int HeroDexterity;
        int HeroVitality;
        int HeroLuck;
        int HeroAgility;

};

#endif // ENEMY_H_INCLUDED
