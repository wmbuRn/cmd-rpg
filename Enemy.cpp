#include "Enemy.h"

Enemy::Enemy(int LevelOfHero, int StrenghtOfHero, int DexterityOfHero, int LuckOfHero, int VitalityOfHero, int AgilityOfHero)
{
    /*
        Let's feed variable from Hero class we get on constructing Enemy class
    */
    HeroLevel = LevelOfHero;
    HeroStrenght = StrenghtOfHero;
    HeroDexterity = DexterityOfHero;
    HeroLuck = LuckOfHero;
    HeroVitality = VitalityOfHero;
    HeroAgility = AgilityOfHero;

}

Enemy::~Enemy()
{
    // No need for d-con
}

int Enemy::getMaximumHealth()
{
    return ( generateRandomNumbers( HeroStrenght ) / 2 ) + ( generateRandomNumbers(HeroVitality * 4) );
}

int Enemy::getMinimumDMG()
{
    int DMG;
    DMG = ( generateRandomNumbers( HeroStrenght ) / 3) + (generateRandomNumbers( HeroAgility ) / 5 ) + ( generateRandomNumbers( HeroLuck ) / 3 );
    return DMG;
}

int Enemy::getMaximumDMG()
{
    int DMG;
    DMG = ( generateRandomNumbers( HeroStrenght ) / 2) + (generateRandomNumbers( HeroAgility ) / 4 ) + ( generateRandomNumbers( HeroLuck ) / 3 );
    return DMG;
}


int Enemy::generateRandomNumbers(int maxValue)
{
    int minValue = maxValue - rand() % 5;
    /*
        Let's check if minVale is not less than 0
    */
    if ( minValue < 0 )
    {
        minValue = 0;
    }

    /*
        We are going to add 0-5 to hero attribute, so if
        Hero have 10 Strenght, enemy can possible have value between 5-15
    */
    maxValue = maxValue + rand() % 5;

    /*
        initialize random seed
    */
    srand (time(NULL));

    /*
        Give us random number between minimumValue and MaximumValue
    */
    return rand() % maxValue + minValue;

}
